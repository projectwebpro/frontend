export default interface Emp {
  id: number;
  name: string;
  gender: string;
  position: string;
  tel: string;
}
