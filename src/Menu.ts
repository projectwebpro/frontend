export default interface Menu {
  id: number;
  menu: String;
  price: number;
  count: number;
  status: String;
}
