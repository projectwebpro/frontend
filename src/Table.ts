export default interface Table {
  id?: number;
  table?: string;
  seat?: number;
  status?: string;
}
