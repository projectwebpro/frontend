import type Food from "@/types/Food";
import http from "./axios";
function getFoods() {
  return http.get("/foods");
}

function getFoodsByCategory(category: number) {
  return http.get(`/foods/category/${category}`);
}

function saveFood(food: Food & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", food.name);
  formData.append("price", `${food.price}`);
  formData.append("file", food.files[0]);
  formData.append("categoryId", `${food.categoryId}`);
  return http.post("/foods", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateFood(id: number, food: Food & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", food.name);
  formData.append("price", `${food.price}`);
  if (food.files) {
    formData.append("file", food.files[0]);
  }

  return http.patch(`/foods/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteFood(id: number) {
  return http.delete(`/foods/${id}`);
}

export default {
  getFoods,
  saveFood,
  updateFood,
  deleteFood,
  getFoodsByCategory,
};
