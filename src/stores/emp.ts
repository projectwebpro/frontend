import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Emp from "@/types/Emp";
import empService from "@/services/emp";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmpStore = defineStore("Emp", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const emp = ref<Emp[]>([]);
  const editedEmp = ref<Emp>({ name: "", gender: "", position: "", tel: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmp.value = { name: "", gender: "", position: "", tel: "" };
    }
  });
  async function getEmp() {
    loadingStore.isLoading = true;
    try {
      const res = await empService.getEmp();
      emp.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล พนักงาน ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmp() {
    loadingStore.isLoading = true;
    try {
      if (editedEmp.value.id) {
        const res = await empService.updateEmp(
          editedEmp.value.id,
          editedEmp.value
        );
      } else {
        const res = await empService.saveEmp(editedEmp.value);
      }

      dialog.value = false;
      await getEmp();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล พนักงาน ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteEmp(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await empService.deleteEmp(id);
      await getEmp();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล พนักงาน ได้");
    }
    loadingStore.isLoading = false;
  }
  function editEmp(emp: Emp) {
    editedEmp.value = JSON.parse(JSON.stringify(emp));
    dialog.value = true;
  }
  return {
    emp,
    getEmp,
    dialog,
    editedEmp,
    saveEmp,
    editEmp,
    deleteEmp,
  };
});
