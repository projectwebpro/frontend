import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Material from "@/types/Material";
import materialService from "@/services/materail";
export const useMaterialStore = defineStore("Material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const material = ref<Material[]>([]);
  const editedMaterial = ref<Material>({
    name: "",
    minAmount: 10,
    balance: 0,
    unit: "",
  });

  //dialog เเก้ไข name
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMaterial.value = { name: "", minAmount: 0, balance: 0, unit: "" };
    }
  });

  //getStock
  async function getMaterial() {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.getMaterail();
      material.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล วัตถุดิบ ได้");
    }
    loadingStore.isLoading = false;
  }

  //saveMaterail
  async function saveMaterail() {
    loadingStore.isLoading = true;
    try {
      if (editedMaterial.value.id) {
        // editedMaterial.value.balance =
        //   editedMaterial.value.import - editedMaterial.value.export;
        const res = await materialService.updateMaterail(
          editedMaterial.value.id,
          editedMaterial.value
        );
      } else {
        // editedMaterial.value.balance =
        //   editedMaterial.value.import - editedMaterial.value.export;
        const res = await materialService.saveMaterail(editedMaterial.value);
      }

      dialog.value = false;
      await getMaterial();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล วัตถุดิบ ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  //deleteMatial
  async function deleteMaterail(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.deleteMaterail(id);
      await getMaterial();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล วัตถุดิบ ได้");
    }
    loadingStore.isLoading = false;
  }

  function editMaterial(materail: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(materail));
    dialog.value = true;
  }
  return {
    material,
    getMaterial,
    dialog,
    editedMaterial,
    editMaterial,
    saveMaterail,
    deleteMaterail,
  };
});
