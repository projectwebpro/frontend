import { ref } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/Menu";

export const useMenu = defineStore("menu", () => {
  const menu = ref<Menu[]>([
    {
      id: 1,
      menu: "กระเพราไก่ไข่ดาว",
      price: 55,
      count: 2,
      status: "Completed",
    },
    { id: 2, menu: "สุกี้ทะเลรวม", price: 65, count: 1, status: "Completed" },
    { id: 3, menu: "ข้าวผัดไข่", price: 50, count: 2, status: "Pending" },
    { id: 4, menu: "ราดหน้าหมู", price: 55, count: 1, status: "Pending" },
  ]);
  return {
    menu,
  };
});
