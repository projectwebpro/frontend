import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Quefoos from "@/types/Quefood";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import QueFoodService from "@/services/quefood";
import type QueFood from "@/types/Quefood";
import type QueueSaveOrderItem from "@/types/QueSaveOrderItem";
import quefood from "@/services/quefood";
export const useCookStore = defineStore("quecook", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const quefoodstatus = ref<QueFood[]>([]); // สถานะตามorderItem
  const quecook = ref<QueFood[]>([]); // ทำอาหาร
  const quefinish = ref<QueFood[]>([]); // เสร็จสิ้น
  const quewait = ref<QueFood[]>([]); // รอเตรียม
  const quefood = ref<QueFood[]>([]); // รอเตรียม
  const editedQuefoods = ref<QueFood>({ status: "" }); //ต้องการเเก้ไขบ้าง
  const editedQuefoodsStatus = ref<QueFood>({ status: "" });
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedQuefoods.value = { status: "" }; // เเก้ไขอะไร
    }
  });

  async function getQuefoods() {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQuefoods();
      quefood.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  //getStatusWait
  async function getQuefoodsStatusWait(status: string) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueueItemByStatus(status);
      quewait.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  //getById
  async function getQueByOrderItemId(idOrderItem: number) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueByOrderItemid(idOrderItem);
      quefoodstatus.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  //getStatusCook
  async function getQuefoodsStatusCook(status: string) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueueItemByStatus(status);
      quecook.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  //getStatusFinish
  async function getQuefoodsStatusFinsh(status: string) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueueItemByStatus(status);
      quefinish.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  // delete
  async function deleteQueWait(id: number) {
    loadingStore.isLoading = true;
    let status: QueFood;
    try {
      const resId = await QueFoodService.getQuefoodsID(id); //ดึง QuefoodId ที่กดทำอาหารมาจากหน้าบ้าน
      status = resId.data;
      status.status = "อาหารถูกยกเลิก";
      QueFoodService.updateQuefoods(id, status); // update status// เปลี่ยน status เป็นอาหารถูกยกยกเลิก ใน database
      //const res = await QueFoodService.deleteQuefoods(id);
      const index = quewait.value.findIndex((item) => item.id === id);
      quewait.value.splice(index, 1);
      // await getQuefoods();
      for (let i = 0; i < quefoodstatus.value.length; i++) {
        if (quefoodstatus.value[i].id === id) {
          quefoodstatus.value[i].status = "อาหารถูกยกเลิก";
        }
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteQueCook(id: number) {
    loadingStore.isLoading = true;
    let status: QueFood;
    try {
      const index = quecook.value.findIndex((item) => item.id === id);
      const resId = await QueFoodService.getQuefoodsID(id); //ดึง QuefoodId ที่กดทำอาหารมาจากหน้าบ้าน
      //const res = await QueFoodService.deleteQuefoods(id);
      status = resId.data;
      status.status = "อาหารถูกยกเลิก";
      QueFoodService.updateQuefoods(id, status); // update status// เปลี่ยน status เป็นอาหารถูกยกยกเลิก ใน database
      quecook.value.splice(index, 1);
      // await getQuefoods();
      for (let i = 0; i < quefoodstatus.value.length; i++) {
        if (quefoodstatus.value[i].id === id) {
          quefoodstatus.value[i].status = "อาหารถูกยกเลิก";
        }
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }
  function editQueFood(quefood: Quefoos) {
    // editedQuefoods.value = JSON.parse(JSON.stringify(quecook));
    dialog.value = true;
  }

  async function cookingQue(id: number) {
    // กดทำอาหาร
    let status: QueFood;
    try {
      ///หลังบ้าน
      const res = await QueFoodService.getQuefoodsID(id); //ดึง QuefoodId ที่กดทำอาหารมาจากหน้าบ้าน
      status = res.data;
      status.status = "ทำอาหาร"; // เปลี่ยน status เป็นทำอาหาร ใน database
      QueFoodService.updateQuefoods(id, status); // update status
      //หน้าบ้าน
      const index = quewait.value.findIndex((item) => item.id === id);
      quecook.value.push(quewait.value[index]);
      quewait.value.splice(index, 1);
      for (let i = 0; i < quefoodstatus.value.length; i++) {
        if (quefoodstatus.value[i].id === id) {
          quefoodstatus.value[i].status = "ทำอาหาร";
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function finishQue(id: number) {
    //กดเสร็จสิ้น
    let status: QueFood;
    try {
      const res = await QueFoodService.getQuefoodsID(id); //ดึง QuefoodId ที่กดทำมาจากหน้าบ้าน
      status = res.data;
      status.status = "รอเสิร์ฟ"; // เปลี่ยน status เป็นรอเสิร์ฟ ใน database
      QueFoodService.updateQuefoods(id, status); // update status
      //หน้าบ้าน
      const index = quecook.value.findIndex((item) => item.id === id);
      quefinish.value.push(quecook.value[index]);
      quecook.value.splice(index, 1);

      for (let i = 0; i < quefoodstatus.value.length; i++) {
        if (quefoodstatus.value[i].id === id) {
          quefoodstatus.value[i].status = "รอเสิร์ฟ";
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function getQuefoodsCook(status: string) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueueItemByStatus(status);
      quecook.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  //Update
  async function saveQuefoods() {
    loadingStore.isLoading = true;
    try {
      if (editedQuefoods.value.id) {
        const res = await QueFoodService.updateQuefoods(
          editedQuefoods.value.id,
          editedQuefoods.value
        );
      } else {
        const res = await QueFoodService.saveQuefoods(editedQuefoods.value);
      }
      dialog.value = false;
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล คิวอาหาร ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function UpdateStatusCooking(id: number) {
    // อัพเดตstatuscooking
    let status: QueFood;
    try {
      const res = await QueFoodService.getQuefoodsID(id);
      status = res.data;
      status.status = "ทำอาหาร";
      QueFoodService.updateQuefoods(id, status);
      const index = quewait.value.findIndex((item) => item.id === id);
      // quecook.value.push(quewait.value[index]);
      quecook.value.push(quewait.value[index]);
      quewait.value.splice(index, 1);
      for (let i = 0; i < quefoodstatus.value.length; i++) {
        if (quefoodstatus.value[i].id === id) {
          quefoodstatus.value[i].status = "ทำอาหาร";
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
  function ClearListFinish() {
    quefinish.value = [];
  }

  const iditemCancleWait = ref(0);
  const dialogCancleWait = ref(false);
  const namefoodCancle = ref("");

  const DialogCancleQueWait = (
    id: number,
    namefood: string,
    idTable: number
  ) => {
    dialogCancleWait.value = true;
    namefoodCancle.value = namefood;
    iditemCancleWait.value = idTable;
    deleteQueCook(id);
  };
  return {
    quecook,
    quewait,
    quefinish,
    deleteQueWait,
    getQuefoodsCook,
    cookingQue,
    finishQue,
    getQuefoods,
    saveQuefoods,
    editedQuefoods,
    // UpdateStatusCooking,
    getQuefoodsStatusWait,
    deleteQueCook,
    getQuefoodsStatusCook,
    getQuefoodsStatusFinsh,
    ClearListFinish,
    getQueByOrderItemId,
    quefoodstatus,
    quefood,
    editedQuefoodsStatus,
    iditemCancleWait,
    dialogCancleWait,
    namefoodCancle,
    DialogCancleQueWait,
  };
});
