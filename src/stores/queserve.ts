import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type QueServe from "@/QueServe";
import type QueCook from "@/QueCook";
import { useCookStore } from "@/stores/quefood";
import type QueFood from "@/types/Quefood";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import QueFoodService from "@/services/quefood";
const queStore = useCookStore();
const loadingStore = useLoadingStore();
const messageStore = useMessageStore();
export const useServeStore = defineStore("queserve", () => {
  const queserve = ref<QueFood[]>([]); // รอเตรียม
  const editedQuefoods = ref<QueFood>({ status: "" }); //ต้องการเเก้ไขบ้าง
  const dialog = ref(false);
  async function getQuefoods() {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQuefoods();
      queserve.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedQuefoods.value = { status: "" }; // เเก้ไขอะไร
    }
  });

  //getStatusWait
  async function getQuefoodsStatusServe(status: string) {
    loadingStore.isLoading = true;
    try {
      const res = await QueFoodService.getQueueItemByStatus(status);
      queserve.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล คิวอาหาร ได้");
    }
    loadingStore.isLoading = false;
  }

  async function serveQue(id: number, index: number) {
    // loadingStore.isLoading = true;
    // let status: QueFood;
    // queserve.value.splice(id, 1); // ลบตำเเหร่งที่อยู่ในคิวเสิร์ฟ

    let status: QueFood;
    try {
      ///หลังบ้าน
      const res = await QueFoodService.getQuefoodsID(id); //ดึง QuefoodId ที่กดทำอาหารมาจากหน้าบ้าน
      status = res.data;
      status.status = "เสิร์ฟเเล้ว"; // เปลี่ยน status เป็นทำอาหาร ใน database
      QueFoodService.updateQuefoods(id, status); // update status

      //หน้าบ้าน
      //const index = queserve.value.findIndex((item) => item.id === id);
      queserve.value.splice(index, 1);

      for (let i = 0; i < queStore.quefoodstatus.length; i++) {
        if (queStore.quefoodstatus[i].id === id) {
          queStore.quefoodstatus[i].status = "เสิร์ฟเเล้ว";
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  return {
    queserve,
    editedQuefoods,
    getQuefoods,
    getQuefoodsStatusServe,
    serveQue,
  };
});
