export default interface Material {
  id?: number;
  name: string;
  minAmount?: number;
  unit?: string;
  balance?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
