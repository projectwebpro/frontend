import type Table from "@/Table";
import type OrderItem from "./OrderItem";

export default interface Order {
  id?: number;
  amount?: number;
  total?: number;
  // customer: Customer;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
  orderItems?: OrderItem[];
  table?: Table;
}
