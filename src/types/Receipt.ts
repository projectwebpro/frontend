import type ReceiptItem from "./ReceiptItem";

export default interface Receipt {
  id?: number;
  total?: number; //เงินรวม
  receive?: number; //เงินรับ
  change?: number; // เงินทอน
  nameEmp?: string; // ชื่อพนักงาน
  createdDate?: Date; // วันที่สร้าง
  updatedDate?: Date;
  deletedDate?: Date;
  receiptItems?: ReceiptItem[];
}
